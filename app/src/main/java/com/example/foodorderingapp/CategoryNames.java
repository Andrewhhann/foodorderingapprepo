package com.example.foodorderingapp;

public enum CategoryNames {
    BREAKFAST("Breakfast"),
    LUNCH("Lunch"),
    DINNER("Dinner"),
    BEVERAGES("Beverages"),
    INTERNATIONAL("International"),
    APPETIZER("Appetizer"),
    DESSERT("Dessert");

    //Constructor
    CategoryNames(String categoryName) {
        this.categoryName = categoryName;
    }

    //Private properties
    private String categoryName;

    @Override
    public String toString() {
        return this.categoryName;
    }
}
