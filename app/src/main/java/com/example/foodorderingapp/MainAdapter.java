package com.example.foodorderingapp;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class MainAdapter extends RecyclerView.Adapter<MainViewHolder> {

    public MainAdapter(@NonNull Context context, @NonNull ArrayList<Restaurant> restaurants) {
        this.context = context;
        this.restaurants = restaurants;
    }

    @NonNull
    @Override
    public MainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View mainItemView = inflater.inflate(R.layout.main_item, parent, false);
        return new MainViewHolder(mainItemView);
    }

    private int getResID(String name) {
        try {
            Class res = R.drawable.class;
            Field field = res.getField(name);
            int drawableId = field.getInt(null);
            return drawableId;
        }
        catch (Exception e) {
            Log.e("MyTag", "Failure to get drawable id.", e);
        }
        return 0;
    }
    @Override
    public void onBindViewHolder(@NonNull MainViewHolder holder, int position) {
        Restaurant restaurant = restaurants.get(position);
        //holder.mainItemNameTextView.setText(restaurant.name + "");
        String pkg = context.getPackageName();
        int resID = context.getResources().getIdentifier(restaurant.imageResourceName, "drawable", context.getPackageName());
        //int resID2 = getResID(restaurant.imageResourceName);
        holder.mainItemImageView.setImageResource(resID);
        holder.mainItemHeartAmountTextView.setText(Integer.toString(restaurant.rating));
        holder.mainItemNameTextView.setText(restaurant.name);
        holder.restaurantIndex = restaurant.index;
    }

    @Override
    public int getItemCount() {
        return restaurants.size();
    }

    //Private properties
    private final Context context;
    private final ArrayList<Restaurant> restaurants;
}
