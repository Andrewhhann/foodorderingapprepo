package com.example.foodorderingapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        connectingXMLViews();

        addressSpinner.setOnItemSelectedListener(this);


        populateDataModels();

        setupRestaurantNearYouRecyclerView();

        setupYourFavouritesRecyclerView();

        setupWhatsTrendingRecyclerView();

        //Setting up navigation bar
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.home_page:
                        System.out.println("Already in home screen.");
                        return true;
                    case R.id.search_page:
                        navigateToSearchScreen();
                        System.out.println("Search screen clicked.");
                        return true;
                    case R.id.cart_page:
                        navigateToCartScreen();
                        System.out.println("Cart screen clicked.");
                        return true;
                    case R.id.save_page:
                        navigateToSaveScreen();
                        System.out.println("Save screen clicked.");
                        return true;
                    case R.id.profile_page:
                        navigateToProfileScreen();
                        System.out.println("Profile screen clicked.");
                }
                return false;
            }
        });


        breakfastButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                curSelectedCategory = CategoryNames.BREAKFAST;
                navigateToCategoryScreen();
            }
        });

        lunchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                curSelectedCategory = CategoryNames.LUNCH;
                navigateToCategoryScreen();
            }
        });

        dinnerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                curSelectedCategory = CategoryNames.DINNER;
                navigateToCategoryScreen();
            }
        });

        beveragesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                curSelectedCategory = CategoryNames.BEVERAGES;
                navigateToCategoryScreen();
            }
        });

        internationalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                curSelectedCategory = CategoryNames.INTERNATIONAL;
                navigateToCategoryScreen();
            }
        });

        appetizerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                curSelectedCategory = CategoryNames.APPETIZER;
                navigateToCategoryScreen();
            }
        });

        dessertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                curSelectedCategory = CategoryNames.DESSERT;
                navigateToCategoryScreen();
            }
        });

    }

    public CategoryNames getCurSelectedCategory() {
        return curSelectedCategory;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Toast.makeText(this, adapterView.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    //Private methods
    private void connectingXMLViews(){
        bottomNavigationView = findViewById(R.id.bottom_navigation_bar);
        addressSpinner = findViewById(R.id.address_dropdown);
        restaurantsNearYouRecyclerView = findViewById(R.id.restaurantsNearYou_recyclerView);
        yourFavouritesRecyclerView = findViewById(R.id.yourFavourites_recyclerView);
        whatsTrendingRecyclerView = findViewById(R.id.whatsTrending_recyclerView);
        breakfastButton = findViewById(R.id.breakfast_button);
        lunchButton = findViewById(R.id.lunch_button);
        dinnerButton = findViewById(R.id.dinner_button);
        beveragesButton = findViewById(R.id.beverages_button);
        internationalButton = findViewById(R.id.international_button);
        appetizerButton = findViewById(R.id.appetizer_button);
        dessertButton = findViewById(R.id.dessert_button);

    }

    private void populateDataModels() {
        category = new Category();
        category.populateCategory();
    }

    private void setupRestaurantNearYouRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.HORIZONTAL);
        restaurantsNearYouRecyclerView.setLayoutManager(layoutManager);
        MainAdapter mainAdapter = new MainAdapter(this, pickFourRandomRestaurants());
        restaurantsNearYouRecyclerView.setAdapter(mainAdapter);
    }

    private void setupYourFavouritesRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.HORIZONTAL);
        yourFavouritesRecyclerView.setLayoutManager(layoutManager);
        MainAdapter mainAdapter = new MainAdapter(this, pickFourRandomRestaurants());
        yourFavouritesRecyclerView.setAdapter(mainAdapter);
    }

    private void setupWhatsTrendingRecyclerView () {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.HORIZONTAL);
        whatsTrendingRecyclerView.setLayoutManager(layoutManager);
        MainAdapter mainAdapter = new MainAdapter(this, pickFourRandomRestaurants());
        whatsTrendingRecyclerView.setAdapter(mainAdapter);
    }

    private ArrayList<Restaurant> pickFourRandomRestaurants () {
        ArrayList<Restaurant> randomRestaurants = new ArrayList<>();
        for (int i = 0; i <4; i++) {
            int id = (int)(Math.random() * category.restaurants.size());
            randomRestaurants.add(category.restaurants.get(id));
        }

        return randomRestaurants;
    }


    //Connecting XML
    private BottomNavigationView bottomNavigationView;
    private Spinner addressSpinner;
    private RecyclerView restaurantsNearYouRecyclerView;
    private RecyclerView yourFavouritesRecyclerView;
    private RecyclerView whatsTrendingRecyclerView;
    private ImageButton breakfastButton;
    private ImageButton lunchButton;
    private ImageButton dinnerButton;
    private ImageButton beveragesButton;
    private ImageButton internationalButton;
    private ImageButton appetizerButton;
    private ImageButton dessertButton;
    public static CategoryNames curSelectedCategory;
    public static Category category;

    private void navigateToCategoryScreen() {
        Intent intent = new Intent(this, CategoryActivity.class);
        startActivity(intent);
    }
    private void navigateToHomeScreen() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    private void navigateToSearchScreen() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }
    private void navigateToCartScreen() {
        Intent intent = new Intent(this, CartActivity.class);
        startActivity(intent);
    }
    private void navigateToSaveScreen() {
        Intent intent = new Intent(this, SaveActivity.class);
        startActivity(intent);
    }
    private void navigateToProfileScreen() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }

}
