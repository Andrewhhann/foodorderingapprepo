package com.example.foodorderingapp;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageButton;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class RestaurantActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restaurant);
        connectXmlViews();

        //Setting up navigation bar
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.home_page:
                        navigateToHomeScreen();
                        System.out.println("Home screen clicked.");
                        return true;
                    case R.id.search_page:
                        navigateToSearchScreen();
                        System.out.println("Search screen clicked.");
                        return true;
                    case R.id.cart_page:
                        navigateToCartScreen();
                        System.out.println("Cart screen clicked.");
                        return true;
                    case R.id.save_page:
                        navigateToSaveScreen();
                        System.out.println("Save screen clicked.");
                        return true;
                    case R.id.profile_page:
                        navigateToProfileScreen();
                        System.out.println("Profile screen clicked.");
                }
                return false;
            }
        });
    }

    // Connect xml
    private BottomNavigationView bottomNavigationView;

    private void connectXmlViews() {
        bottomNavigationView = findViewById(R.id.bottom_navigation_bar);
    }
    // Private methods
    private void navigateToHomeScreen() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    private void navigateToSearchScreen() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }
    private void navigateToCartScreen() {
        Intent intent = new Intent(this, CartActivity.class);
        startActivity(intent);
    }
    private void navigateToSaveScreen() {
        Intent intent = new Intent(this, SaveActivity.class);
        startActivity(intent);
    }
    private void navigateToProfileScreen() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }
}
