package com.example.foodorderingapp;

import java.util.ArrayList;
import java.util.HashMap;

public class Category {

    //public property
    public ArrayList<CategoryItem> categories = new ArrayList<>();
    public ArrayList<Restaurant> restaurants = new ArrayList<>();

    //private method
    public void populateCategory () {
        int catIndex = 0;
        //Iterate through every possible Category type
        for (CategoryNames categoryNames : CategoryNames.values()){

            CategoryItem categoryItem = new CategoryItem();

            categoryItem.name = categoryNames;

            categoryItem.imageResourceName = categoryNames.name().toString() + "_image";

            for (int i = 0; i < 4; i++) {
                int restaurant_index = i + 4 * catIndex;
                String imageResourceName = "restaurant_image_" + Integer.toString(restaurant_index + 1);
                String imageInCate = "ellipse_" + Integer.toString(restaurant_index + 4);
                String imageInRestaurant = "restaurant_in_cate_image_" + Integer.toString(restaurant_index + 1);
                Restaurant restaurant = new Restaurant(imageResourceName, imageInCate, imageInRestaurant, restaurant_index);
                categoryItem.restaurants.add(restaurant);
                restaurants.add(restaurant);
            }

            categories.add(categoryItem);
            ++catIndex;
        }

        System.out.println("Categories" + categories);
    }

    public CategoryItem findCategoryItem(CategoryNames name) {
        for (int i = 0; i < categories.size(); i++) {
             CategoryItem item = categories.get(i);
             if (item.name == name)
                 return item;
        }
        return null;
    }

    //Nested class
    public class CategoryItem {

        //public properties
        //public String name;
        public String imageResourceName;
        public CategoryNames name;
        public ArrayList<Restaurant> restaurants = new ArrayList<>();
    }
}


