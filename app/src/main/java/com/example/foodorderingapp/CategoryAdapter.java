package com.example.foodorderingapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryViewHolder> {

    public CategoryAdapter(@NonNull Context context) {
        this.context = context;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Get current selected category.
        if (categoryItem == null) {
            Category category = MainActivity.category;
            categoryItem = category.findCategoryItem(MainActivity.curSelectedCategory);
        }

        LayoutInflater inflater = LayoutInflater.from(context);
        View cateItemView = inflater.inflate(R.layout.category_save_item, parent, false);
        return new CategoryViewHolder(cateItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        Restaurant restaurant = categoryItem.restaurants.get(position);
        int resID = context.getResources().getIdentifier(restaurant.imageNameInCate, "drawable", context.getPackageName());
        holder.cateItemImageView.setImageResource(resID);
        holder.cateItemRateTextView.setText(Integer.toString(restaurant.rating));
        holder.cateItemNameTextView.setText(restaurant.name);
        holder.cateItemDescriptionTextView.setText(lorem.getWords(2, 5));
    }

    @Override
    public int getItemCount() {
        if (categoryItem == null) {
            categoryItem = MainActivity.category.findCategoryItem(MainActivity.curSelectedCategory);
        }

        if (categoryItem == null) {
            return 0;
        } else {
            return categoryItem.restaurants.size();
        }
    }

    //Private properties
    private Lorem lorem = LoremIpsum.getInstance();
    private Category.CategoryItem categoryItem;
    private Context context;
}
