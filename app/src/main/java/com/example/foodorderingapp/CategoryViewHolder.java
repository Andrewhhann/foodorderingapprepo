package com.example.foodorderingapp;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CategoryViewHolder extends RecyclerView.ViewHolder {
    public ImageView cateItemImageView;
    public TextView cateItemNameTextView;
    public TextView cateItemDescriptionTextView;
    public TextView cateItemRateTextView;

    public CategoryViewHolder(View cateItemView) {
        super(cateItemView);
        cateItemImageView = cateItemView.findViewById(R.id.breakfast_restaurants_image);
        cateItemNameTextView = cateItemView.findViewById(R.id.breakfast_restaurants_name);
        cateItemDescriptionTextView = cateItemView.findViewById(R.id.breakfast_restaurants_description);
        cateItemRateTextView = cateItemView.findViewById(R.id.breakfast_restaurants_rating);
    }
}
