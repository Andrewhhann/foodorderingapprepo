package com.example.foodorderingapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.w3c.dom.Text;

public class CategoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category);

        connectingXMLViews();

        populateDataModel();

        setupRestaurantRecyclerView();

        //Setting up navigation bar
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.home_page:
                        System.out.println("Already in home screen.");
                        return true;
                    case R.id.search_page:
                        navigateToSearchScreen();
                        System.out.println("Search screen clicked.");
                        return true;
                    case R.id.cart_page:
                        navigateToCartScreen();
                        System.out.println("Cart screen clicked.");
                        return true;
                    case R.id.save_page:
                        navigateToSaveScreen();
                        System.out.println("Save screen clicked.");
                        return true;
                    case R.id.profile_page:
                        navigateToProfileScreen();
                        System.out.println("Profile screen clicked.");
                }
                return false;
            }
        });

    }

    //private methods
    private void connectingXMLViews () {
        bottomNavigationView = findViewById(R.id.bottom_navigation_bar);
        categoryImage = findViewById(R.id.top_breakfast_image);
        categoryName = findViewById(R.id.categoryName);
        restaurantCount = findViewById(R.id.restaurant_count);
        restaurantsView = findViewById(R.id.restaurants_view);
    }

    private void populateDataModel () {
        CategoryNames name = MainActivity.curSelectedCategory;
        Category.CategoryItem item = MainActivity.category.findCategoryItem(name);
        categoryName.setText(name.toString());
        restaurantCount.setText(Integer.toString(item.restaurants.size()) + " Restaurants");
        if (name == CategoryNames.BREAKFAST) {
            categoryImage.setImageResource(R.drawable.rectangle_4);
        }
    }

    private void setupRestaurantRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        restaurantsView.setLayoutManager(layoutManager);
        CategoryAdapter categoryAdapter = new CategoryAdapter(this);
        restaurantsView.setAdapter(categoryAdapter);
    }
    //ConnectXML
    private BottomNavigationView bottomNavigationView;
    private ImageView categoryImage;
    private TextView categoryName;
    private TextView restaurantCount;
    private RecyclerView restaurantsView;

    private void navigateToCategoryScreen() {
        Intent intent = new Intent(this, CategoryActivity.class);
        startActivity(intent);
    }
    private void navigateToHomeScreen() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    private void navigateToSearchScreen() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }
    private void navigateToCartScreen() {
        Intent intent = new Intent(this, CartActivity.class);
        startActivity(intent);
    }
    private void navigateToSaveScreen() {
        Intent intent = new Intent(this, SaveActivity.class);
        startActivity(intent);
    }
    private void navigateToProfileScreen() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }
}