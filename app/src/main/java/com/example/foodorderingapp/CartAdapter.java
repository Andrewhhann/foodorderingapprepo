package com.example.foodorderingapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class CartAdapter extends RecyclerView.Adapter<CartViewHolder> {

    public CartAdapter(@NonNull Context context, @NonNull ArrayList<Cart.CartItem> cartItems) {
        this.context = context;
        this.cartItems = cartItems;
    }

    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.cart_item, parent, false);
        return new CartViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull com.example.foodorderingapp.CartViewHolder holder, int position) {
        Cart.CartItem cartItem = cartItems.get(position);
        holder.itemQuantityTextView.setText(cartItem.getQuantity().toString());
        holder.itemNameTextView.setText(cartItem.dishName());
        Double subtotal = cartItem.subtotalPriceInCents() / 100.0;
        holder.itemPriceTextView.setText(String.format("$%.2f", subtotal));
        holder.itemImageView.setImageResource(context.getResources().getIdentifier(cartItem.dishImage(), "drawable", context.getApplicationInfo().packageName));
    }

    @Override
    public int getItemCount() {
        return cartItems.size();
    }

    // Private properties
    private final Context context;
    private final ArrayList<Cart.CartItem> cartItems;
}
