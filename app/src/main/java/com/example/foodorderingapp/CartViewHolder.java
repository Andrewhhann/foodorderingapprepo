package com.example.foodorderingapp;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class CartViewHolder extends RecyclerView.ViewHolder{
    public final ImageView itemImageView;
    public final TextView itemNameTextView;
    public final TextView itemPriceTextView;
    public final TextView itemQuantityTextView;
    public final ImageButton itemMinusButton;
    public final ImageButton itemAddButton;

    public CartViewHolder(@NonNull View itemView) {
        super(itemView);
        itemImageView = itemView.findViewById(R.id.cart_item_image);
        itemNameTextView = itemView.findViewById(R.id.cart_item_name);
        itemPriceTextView = itemView.findViewById(R.id.cart_item_subtotal);
        itemQuantityTextView = itemView.findViewById(R.id.cart_item_quantity);
        itemMinusButton = itemView.findViewById(R.id.cart_item_minusButton);
        itemAddButton = itemView.findViewById(R.id.cart_item_addButton);
    }


}
