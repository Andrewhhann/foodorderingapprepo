package com.example.foodorderingapp;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MainViewHolder extends RecyclerView.ViewHolder {
    public ImageView mainItemImageView;
    public TextView mainItemNameTextView;
    public TextView mainItemHeartAmountTextView;
    public int restaurantIndex;

    public MainViewHolder(@NonNull View mainItemView) {
        super(mainItemView);
        mainItemImageView = mainItemView.findViewById(R.id.main_item_image);
        mainItemNameTextView = mainItemView.findViewById(R.id.main_item_text);
        mainItemHeartAmountTextView = mainItemView.findViewById(R.id.main_item_reviews);
    }
}
