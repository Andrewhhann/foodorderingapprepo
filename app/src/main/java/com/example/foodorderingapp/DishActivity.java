package com.example.foodorderingapp;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class DishActivity extends AppCompatActivity {

    int quantityNumber = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dish);
        connectXmlViews();


        subtractButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Subtract button clicked.");
                if (quantityNumber > 1) {
                    quantityNumber --;
                }
                String quantityString = Integer.valueOf(quantityNumber).toString();
                dishQuantity.setText(quantityNumber);
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Add button clicked.");
                if (quantityNumber > 0) {
                    quantityNumber ++;
                }
                String quantityString = Integer.valueOf(quantityNumber).toString();
                dishQuantity.setText(quantityNumber);
            }
        });

        addToCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCart();
            }
        });

        //Setting up navigation bar
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.home_page:
                        navigateToHomeScreen();
                        System.out.println("Home screen clicked.");
                        return true;
                    case R.id.search_page:
                        navigateToSearchScreen();
                        System.out.println("Search screen clicked.");
                        return true;
                    case R.id.cart_page:
                        navigateToCartScreen();
                        System.out.println("Cart screen clicked.");
                        return true;
                    case R.id.save_page:
                        navigateToSaveScreen();
                        System.out.println("Save screen clicked.");
                        return true;
                    case R.id.profile_page:
                        navigateToProfileScreen();
                        System.out.println("Profile screen clicked.");
                }
                return false;
            }
        });

    }

    // Connecting xml
    private ImageView dishImage;
    private TextView dishName;
    private TextView dishPrice;
    private TextView dishDescription;
    private TextView dishQuantity;
    private TextView dishPriceButtonText;
    private Button subtractButton;
    private Button addButton;
    private ImageButton addToCartButton;
    private BottomNavigationView bottomNavigationView;

    private void connectXmlViews() {
        dishImage = findViewById(R.id.dish_image);
        dishName = findViewById(R.id.dish_name);
        dishPrice = findViewById(R.id.dish_price);
        dishDescription = findViewById(R.id.dish_description);
        dishQuantity = findViewById(R.id.dish_quantity);
        dishPriceButtonText = findViewById(R.id.dish_price_button_text);
        subtractButton = findViewById(R.id.subtract_quantity);
        addButton = findViewById(R.id.add_quantity);
        addToCartButton = findViewById(R.id.add_to_cart_button);
        bottomNavigationView = findViewById(R.id.bottom_navigation_bar);
    }

    // Private properties
    private Dish selectedDish;
    private Integer selectedQuantity = quantityNumber;

    //Private methods
    private void navigateToHomeScreen() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    private void navigateToSearchScreen() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }
    private void navigateToCartScreen() {
        Intent intent = new Intent(this, CartActivity.class);
        startActivity(intent);
    }
    private void navigateToSaveScreen() {
        Intent intent = new Intent(this, SaveActivity.class);
        startActivity(intent);
    }
    private void navigateToProfileScreen() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }
    private void addToCart() {
        if (selectedQuantity == 0) {
            // Display an alert message if nothing to add to cart
            new AlertDialog.Builder(this)
                    .setTitle(R.string.cannot_add_to_cart)
                    .setMessage(R.string.choose_one_or_more_items)
                    .setPositiveButton(R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
        else {
            // Add the selected dish and quantity to the cart
            Cart.getInstance().add(selectedDish, selectedQuantity);

            // Dismisses this activity (goes back to previous screen)
            finish();
        }
    }

}
