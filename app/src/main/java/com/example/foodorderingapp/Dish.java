package com.example.foodorderingapp;

import androidx.annotation.NonNull;

import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;

import java.util.ArrayList;

public class Dish {

    //Public properties
    public String name;
    public String description;
    public Integer priceInCents;
    public String imageResourceName;

    //Constructor
    public Dish(String imageResourceName) {
        this.imageResourceName = imageResourceName;

        populateProperties();
    }

    // Public methods
    @Override
    public String toString() {
        return "Dish{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", priceInCents=" + priceInCents +
                ", imageResourceName='" + imageResourceName + '\'' +
                '}';
    }

    public Boolean equals(Dish dish) {
        return (name.equals(dish.name) &&
                description.equals(dish.description) &&
                imageResourceName.equals(dish.imageResourceName) &&
                priceInCents == dish.priceInCents);
    }

    // Private properties
    private static Lorem lorem = LoremIpsum.getInstance();
    private final int maxPriceInCents = 2499;
    private final int minPriceInCents = 499;

    // Private methods
    private void populateProperties() {
        String name = lorem.getTitle(1, 3);
        String description = lorem.getParagraphs(1, 2);

        this.name = name;
        this.description = description;
        this.priceInCents = (int)(Math.random() * (maxPriceInCents - minPriceInCents + 1) + minPriceInCents);
    }
}
