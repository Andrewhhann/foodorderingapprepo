package com.example.foodorderingapp;

import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;

public class Restaurant {

    //Public properties
    public String name;
    public String shortDescription;
    public Integer priceRange;
    public String deliveryTime;
    public String streetAddress;
    public String imageResourceName;
    public String imageNameInCate;
    public String imageNameInRestaurant;
    public int index;
    public int rating;

    //Constructor
    public Restaurant(String imageResourceName, String imageNameInCate, String imageNameInRestaurant, int index) {
        this.imageResourceName = imageResourceName;
        this.imageNameInCate = imageNameInCate;
        this.imageNameInRestaurant = imageNameInRestaurant;
        this.index = index;

        populateProperties();
    }

    //Public methods
    @Override
    public String toString() {
        return "Restaurant{" +
                "name='" + name + '\'' +
                ", shortDescription='" + shortDescription + '\'' +
                ", priceRange=" + priceRange +
                ", deliveryTime='" + deliveryTime + '\'' +
                ", streetAddress='" + streetAddress + '\'' +
                ", imageResourceName='" + imageResourceName + '\'' +
                '}';
    }

    //Private properties
    private Lorem lorem = LoremIpsum.getInstance();
    private final int minPriceRange = 1;
    private final int maxPriceRange = 3;

    //Private method
    private void populateProperties() {
        int minHeart = 100;
        int maxHeart = 2000;
        String name = lorem.getTitle(2, 3);
        String shortDescription = lorem.getWords(1, 3);
        String streetAddress = randomStreetAddress();
        Integer priceRange = (int) (Math.random() * (maxPriceRange - minPriceRange + 1) + minPriceRange);
        String deliveryTime = randomDeliveryTime();

        this.name = name;
        this.shortDescription = shortDescription;
        this.streetAddress = streetAddress;
        this.priceRange = priceRange;
        this.deliveryTime = deliveryTime;
        this.rating = (int) (Math.random() * (maxHeart - minHeart + 1) + minHeart);
    }

    private String randomStreetAddress() {
        int minStreetAddress = 100;
        int maxStreetAddress = 9999;
        int randomNumber = (int) (Math.random() * (maxStreetAddress - minStreetAddress + 1) + minStreetAddress);
        return randomNumber + " " + lorem.getWords(1, 2) + " " + "Street, " + lorem.getCity() + ", " + lorem.getCountry();
    }

    private String randomDeliveryTime() {
        int minDeliveryTime = 5;
        int maxDeliveryTime = 45;
        int randomNumber = (int) (Math.random() * (maxDeliveryTime - minDeliveryTime + 1) + minDeliveryTime);
        return randomNumber + " " + "mins";
    }
}