package com.example.foodorderingapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class CartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart);

        connectXMLViews();

        setUpRecyclerView();

        setupTotalTextView();

        setupCheckoutButton();

        //Setting up navigation bar
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.home_page:
                        navigateToHomeScreen();
                        System.out.println("Home screen clicked.");
                        return true;
                    case R.id.search_page:
                        navigateToSearchScreen();
                        System.out.println("Search screen clicked.");
                        return true;
                    case R.id.cart_page:
                        System.out.println("Already in cart screen.");
                        return true;
                    case R.id.save_page:
                        navigateToSaveScreen();
                        System.out.println("Save screen clicked.");
                        return true;
                    case R.id.profile_page:
                        navigateToProfileScreen();
                        System.out.println("Profile screen clicked.");
                }
                return false;
            }
        });
    }

    //Private methods
    private void setupTotalTextView() {
        double subtotal = Cart.getInstance().totalPriceInCents() / 100.0;
        subtotalTextView.setText(String.format("$%.2f", subtotal));

        double taxes = Cart.getInstance().totalPriceInCents() * 0.12;
        taxesTextView.setText(String.format("$%.2f", taxes));

        double total = subtotal + taxes + 5.0;
        totalTextView.setText(String.format("$%.2f", total));
    }

    private void setupCheckoutButton() {
        final Context context = this;
        checkoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(context)
                        .setTitle(R.string.checkout)
                        .setMessage(R.string.thank_you_for_your_purchase)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Cart.getInstance().clear();
                                finish();
                            }
                        })
                        .setIcon(R.drawable.checkmark)
                        .show();
            }
        });
    }

    private void setUpRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        CartAdapter cartAdapter = new CartAdapter(this, Cart.getInstance().getCartItems());
        recyclerView.setAdapter(cartAdapter);
    }

    //Connecting xml
        private ImageButton checkoutButton;
        private RecyclerView recyclerView;
        private TextView subtotalTextView;
        private TextView totalTextView;
        private TextView taxesTextView;
        private BottomNavigationView bottomNavigationView;
        private ImageButton cartItemAddButton;
        private ImageButton cartItemMinusButton;


        private void connectXMLViews() {
            checkoutButton = findViewById(R.id.checkOutButton);
            recyclerView = findViewById(R.id.cart_recyclerView);
            subtotalTextView = findViewById(R.id.cart_subtotal);
            taxesTextView = findViewById(R.id.cart_taxes);
            totalTextView = findViewById(R.id.cart_total);
            bottomNavigationView = findViewById(R.id.bottom_navigation_bar);
            cartItemAddButton = findViewById(R.id.cart_item_addButton);
            cartItemMinusButton = findViewById(R.id.cart_item_minusButton);
        }

        //Private methods
    private void navigateToHomeScreen() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    private void navigateToSearchScreen() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }
    private void navigateToCartScreen() {
        Intent intent = new Intent(this, CartActivity.class);
        startActivity(intent);
    }
    private void navigateToSaveScreen() {
        Intent intent = new Intent(this, SaveActivity.class);
        startActivity(intent);
    }
    private void navigateToProfileScreen() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }




}
